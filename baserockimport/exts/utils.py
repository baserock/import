# -*- coding: utf-8 -*-
#
# Copyright © 2014, 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from __future__ import print_function

import json
import sys

def warn(*args, **kwargs):
    print('%s:' % sys.argv[0], *args, file=sys.stderr, **kwargs)

def error(*args, **kwargs):
    warn(*args, **kwargs)
    sys.exit(1)

def get_compression(url):
    ''' Given a tarball url, return its compression type '''

    bzip = 'bzip2'
    gzip = 'gzip'
    lzma = 'lzma'

    m = {'tar.gz': gzip, 'tgz': gzip, 'tar.Z': gzip,
           'tar.bz2': bzip, 'tbz2': bzip,
           'tar.lzma': lzma, 'tar.xz': lzma, 'tlz': lzma, 'txz': lzma}

    for x in [1, 2]:
        ext = '.'.join(url.split('.')[-x:])
        if ext in m: return m[ext]

    return None

def str_archive_lorry(archive_type, extname, lorry_prefix, package_name, url):
    ''' Create a json lorry file from a url

        Assumption: url passed to this function must have either:
            * a 'standard' tar extension
            * a zip extension
    '''

    assert archive_type in ('tarball', 'zip')

    name = '%s/%s' % (lorry_prefix, package_name)

    lorry = {'type': archive_type,
             'url': url,
             'x-products-%s' % extname: [package_name]}

    compression = get_compression(url)
    if compression:
        lorry['compression'] = compression

    return json.dumps({name + "-%s" % archive_type: lorry},
                      indent=4, sort_keys=True)

def str_tarball_lorry(extname, lorry_prefix, package_name, url):
    return str_archive_lorry('tarball', extname, lorry_prefix, package_name, url)

def str_zip_lorry(extname, lorry_prefix, package_name, url):
    return str_archive_lorry('zip', extname, lorry_prefix, package_name, url)

def str_repo_lorry(extname, lorry_prefix, package_name, repo_type, url):
    name = '%s/%s' % (lorry_prefix, package_name)

    lorry = {'type': repo_type,
             'url': url,
             'x-products-%s' % extname: [package_name]}

    if repo_type == 'svn':
        lorry['layout'] = 'standard'

    return json.dumps({name: lorry}, indent=4, sort_keys=True)
