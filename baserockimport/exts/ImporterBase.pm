#!/usr/bin/env perl
# Base class for import extensions written in Perl.
#
# Copyright © 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package ImportExtension;
{
    use 5.020;
    use strict;
    use warnings;

    use Moose;
    use FileHandle;
    use Log::Log4perl qw(get_logger :levels);
    use Log::Dispatch;
    use Scalar::Util qw (looks_like_number);

    sub BUILD
    {
        my $self = shift;

        $self->setup_logging();
    }

    sub setup_logging
    {
        my $self = shift;

        my $fd = looks_like_number($ENV{MORPH_LOG_FD})
                    ? $ENV{MORPH_LOG_FD} : 1;

        my $fh = FileHandle->new_from_fd($fd, "w");
        unless (defined $fh) {
            die "Couldn't obtain file handle from morph log file descriptor";
        }

        my $logger = Log::Log4perl->get_logger(ref $self);
        my $appender = Log::Log4perl::Appender->new(
            "Log::Dispatch::Handle",
            handle => $fh,
            name => 'Handle'
        );

        $appender->layout("Log::Log4perl::Layout::SimpleLayout");
        $logger->add_appender($appender);
        $logger->level($DEBUG);

        $logger->debug("Logging started");
    }

    sub run
    {
        ...
    }
}

1;
