# -*- coding: utf-8 -*-
#
# Copyright © 2014, 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

class Package(object):
    '''A package in the processing queue.

    In order to provide helpful errors, this item keeps track of what
    packages depend on it, and hence of why it was added to the queue.

    '''
    def __init__(self, kind, name, version, ignore_version_field=False):
        self._kind = kind
        self._name = name
        self._version = version
        self._build_system = None
        self.required_by = []
        self.morphology = None
        self.repo_url = None
        self.ref = None
        self.named_ref = None
        self.dependencies = None
        self.is_build_dep = False
        self.match = (self._match if ignore_version_field
                                  else self._match_version)


    def __cmp__(self, other):
        return cmp(self.name, other.name)

    def __repr__(self):
        return '<Package %s-%s>' % (self.name, self.version)

    def __str__(self):
        if len(self.required_by) > 0:
            required_msg = ', '.join(self.required_by)
            required_msg = ', required by: ' + required_msg
        else:
            required_msg = ''
        return '%s-%s%s' % (self.name, self.version, required_msg)

    def add_required_by(self, item):
        self.required_by.append('%s-%s' % (item.name, item.version))

    def _match_version(self, kind, name, version):
        return (self.kind == kind and
                self.name == name and
                self.version == version)

    def _match(self, kind, name, version):
        return self.kind == kind and self.name == name

    @property
    def kind(self):
        return self._kind

    @property
    def name(self):
        return self._name

    @property
    def version(self):
        return self._version

    @property
    def parent(self):
        return self.required_by[0] if len(self.required_by) > 0 else None

    @property
    def build_system(self):
        return self._build_system

    def detect_build_system(self, file_iter):
        '''Automatically detect the build system, if possible.

        If the build system cannot be detected automatically, return None.
        '''

        build_systems = {
            'autotools': [
                'autogen',
                'autogen.sh',
                'configure',
                'configure.ac',
                'configure.in',
                'configure.in.in',
            ],
            'python-distutils': [
                'setup.py'
            ],
            'cpan': [
                'Makefile.PL'
            ],
            'module-build': [
                'Build.PL'
            ],
            'cmake': [
                'CMakeLists.txt',
            ],
            'qmake': [
                '.pro'
            ]
        }

        # Aside from ensuring generated definitions are stable,
        # an explicit ordering allows us to favour one build system
        # over another in the case that a single repo supports more
        # than one build system.
        order = ['autotools', 'python-distutils', 'cpan', 'module-build',
                 'cmake', 'qmake']

        files = set(file_iter)
        for bs in order:
            indicators = set(build_systems[bs])

            if not files.isdisjoint(indicators):
                self._build_system = bs
                return
